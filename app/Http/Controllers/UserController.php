<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
    
    public function login(Request $request){   
        $email = $request->email;
        $password = hash('sha256', $request->password);
        $sql    = "SELECT * FROM users where email = '$email' and password = '$password'";
        $result = DB::select($sql);

        if (count($result) == 0) {
            return response('Usuario no existe', 401)
                  ->header('Content-Type', 'text/plain');
        }else{

            $token = Str::random(60);
            
            DB::update("UPDATE users set api_token = '$token'where email = '".$email."'");
             DB::commit();

             $data = ['token' => $token, 
                    'name' => $result[0]->name,
                    'email' => $email];

             return response($data, 200)
                  ->header('Content-Type', 'application/json');
        } 
    }

    public function register(Request $request){
        $email = $request->email;
        $password = hash('sha256', $request->password);
        $name = $request->name;

        $sql    = "SELECT * FROM users where email = '$email'";
        $result = DB::select($sql);

        if (count($result) > 0) {
            
            return response('Usuario Existente', 401)
                  ->header('Content-Type', 'text/plain');

        }else{

            $sql2    = "INSERT INTO users (name, email, password) values ('$name','$email', '$password')";
            DB::insert($sql2);
            DB::commit();

            return response('Success', 200)
                  ->header('Content-Type', 'text/plain');

        }
    }
    
    public function editUser(Request $request){
        $email = $request->email;
        $password = hash('sha256', $request->password);
        $name = $request->name;

        $sql    = "SELECT * FROM users where email = '$email'";
        $result = DB::select($sql);

        if (count($result) > 0) {
             DB::update("UPDATE users set password = '".$password."', name = '".$name."' where email = '".$email."'");
             DB::commit();

             $data = ['name' => $name,
                     'email' => $email];

            return response( $request, 200)
                  ->header('Content-Type', 'application/json');
        } else {
            return response('Usuario no existe', 401)
                  ->header('Content-Type', 'text/plain');
        }

   
    }
}
